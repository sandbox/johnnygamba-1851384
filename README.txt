
This module exposes the function 'first_time_visit_is_first' to find out if
a user has never come to the site before, this is done setting a cookie in the
browser called 'first_time_visit'.

(function ($) {
  Drupal.behaviors.firstTimeVisit = {
    attach: function (context, settings) {
        $('body', context).once('first_time_visit_processed', function(){
          if (!$.cookie('first_time_visit')) {
          $.cookie('first_time_visit', true, { expires: 5000 });
        }
        else {
          $.cookie('first_time_visit', false);
        }
      });
    }
  };
}(jQuery));
